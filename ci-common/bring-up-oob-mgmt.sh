#!/bin/bash
#

set -x
set -e

# Force Colored output for Vagrant when being run in CI Pipeline
export VAGRANT_FORCE_COLOR=true

check_state(){
if [ "$?" != "0" ]; then
    echo "ERROR Could not bring up last series of devices, there was an error of some kind!"
    exit 1
fi
}

#check for cldemo2 folder submodule
#paths to sim are slightly different. we address that here so dowstream it is all common. artifacts are passed forward.
if [ -d "cldemo2" ]; then
  cp -r cldemo2/simulation simulation_$CI_PROJECT_NAME
else
  cp -r simulation simulation_$CI_PROJECT_NAME
fi

cd simulation_$CI_PROJECT_NAME

#set wbid to project concurrency_id so parallel projects' links do not collide
sed -i "s/wbid\ =\ .*/wbid\ =\ $CONCURRENCY_ID/" Vagrantfile
egrep 'wbid\ =\ ' Vagrantfile

echo "#####################################"
echo "#   Starting the MGMT Server...     #"
echo "#####################################"
vagrant up oob-mgmt-server oob-mgmt-switch netq-ts

echo "#####################################"
echo "#   Status of all simulated nodes   #"
echo "#####################################"
vagrant status

echo "#####################################"
echo "#  30 sec pause to complete reboots #"
echo "#####################################"

sleep 30

exit 0

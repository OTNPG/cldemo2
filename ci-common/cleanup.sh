#!/bin/bash

set -x

cd ./simulation_$CI_PROJECT_NAME

date
vagrant destroy -f /server0/ /leaf/ /spine/ /border/ /fw/

#check that we aren't on cldemo2/master.* project else this will not work because
#the machines are halted from disk image creation
if [ "$CI_PROJECT_NAME" != 'cldemo2' ]
then
  echo "Not on cldemo2 NetQ decomm servers"
  vagrant ssh netq-ts -c "bash /home/cumulus/tests/netq-decommission-inside.sh"
else
  echo "On cldemo2. Check if master.*"
  if [[ "$CI_COMMIT_BRANCH" =~ master ]]
  then
    echo "On cldemo2/master. Do nothing"
  else
    echo "Not on cldemo2/master, we can netq decomm"
    vagrant ssh netq-ts -c "bash /home/cumulus/tests/netq-decommission-inside.sh"
  fi
fi

date
vagrant destroy -f netq-ts oob-mgmt-server oob-mgmt-switch

#clean the gitlab-runners for inside simulations that may be present
echo "Cleanup old oob-mgmt-server inside runners"
RUNNER_IDS=`curl --header "PRIVATE-TOKEN: $API_KEY" "https://gitlab.com/api/v4/runners?tag_list=${CI_PROJECT_NAME}:oob-mgmt" |  jq '.[] | .id'`
for id in $RUNNER_IDS
do
  curl --request DELETE --header "PRIVATE-TOKEN: $API_KEY" "https://gitlab.com/api/v4/runners/$id"
done
echo "Cleanup old netq-ts inside runners"
RUNNER_IDS=`curl --header "PRIVATE-TOKEN: $API_KEY" "https://gitlab.com/api/v4/runners?tag_list=${CI_PROJECT_NAME}:netq-ts" |  jq '.[] | .id'`
for id in $RUNNER_IDS
do
  curl --request DELETE --header "PRIVATE-TOKEN: $API_KEY" "https://gitlab.com/api/v4/runners/$id"
done


echo "Finished."

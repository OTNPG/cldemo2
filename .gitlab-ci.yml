---
stages:
  - lint
  - prep simulation environment
  - oob-mgmt bringup
  - setup inside runners
  - network bringup
  - enable all interfaces
  - test the simulation
  - cleanup the simulation
  - trigger downstream pipelines

lint:
  stage: lint
  script:
    - bash ./ci-common/linter.sh

prep:
  before_script:
    - PRIVATE_TOKEN="$API_KEY" gitlab-job-guard -c=^dev*
  stage: prep simulation environment
  script:
    - bash ./ci-common/prep.sh
  only:
    refs:
      - /^dev.*/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml

build-oob-mgmt:
  stage: oob-mgmt bringup
  script:
    - bash ./ci-common/bring-up-oob-mgmt.sh
  artifacts:
    expire_in: 1 week
    untracked: true
    paths:
      - simulation_$CI_PROJECT_NAME/
  only:
    refs:
      - /^dev.*/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml

setup-runner-on-netq-ts:
  stage: setup inside runners
  script:
    - cd simulation_$CI_PROJECT_NAME
    - vagrant ssh netq-ts -c "sudo gitlab-runner register --non-interactive --url https://gitlab.com --registration-token $RUNNER_REG_TOKEN --description netq-ts:$CI_JOB_ID --tag-list $CI_PROJECT_NAME:netq-ts --executor shell"
  only:
    refs:
      - /^dev.*/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml
  dependencies:
    - build-oob-mgmt

setup-runner-on-oob-mgmt-server:
  stage: setup inside runners
  script:
    - cd simulation_$CI_PROJECT_NAME
    - vagrant ssh oob-mgmt-server -c "sudo gitlab-runner register --non-interactive --url https://gitlab.com --registration-token $RUNNER_REG_TOKEN --description oob-mgmt-server:$CI_JOB_ID --tag-list $CI_PROJECT_NAME:oob-mgmt --executor shell"
  only:
    refs:
      - /^dev.*/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml
  dependencies:
    - build-oob-mgmt

build-network:
  stage: network bringup
  script:
    - bash ./ci-common/bring-up-network.sh
  artifacts:
    expire_in: 1 week
    untracked: true
    paths:
      - simulation_$CI_PROJECT_NAME/
  only:
    refs:
      - /^dev.*/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml
  dependencies:
    - build-oob-mgmt

provision-netq:
  stage: network bringup
  script:
    - bash ./ci-common/provision-opta.sh
  only:
    refs:
      - /^dev.*/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml
  tags:
    - cldemo2:netq-ts

turnup-interfaces:
  stage: enable all interfaces
  script:
    - ansible-playbook ./tests/enable-interfaces.yml
    - ansible exit:leaf:spine:host -a "netq config restart agent"
  only:
    refs:
      - /^dev.*/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml
  tags:
    - cldemo2:oob-mgmt

test-from-oob-server:
  stage: test the simulation
  script:
    - bash ./tests/oob-server-tests.sh
  only:
    refs:
      - /^dev.*/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml
  tags:
    - cldemo2:oob-mgmt

test-from-netq-ts:
  stage: test the simulation
  script:
    - bash ./tests/netq-ts-tests.sh
  only:
    refs:
      - /^dev.*/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml
  tags:
    - cldemo2:netq-ts

cleanup:
  stage: cleanup the simulation
  script:
    - bash ./ci-common/cleanup.sh
  only:
    refs:
      - /^dev.*/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml
  dependencies:
    - build-network

downstream:
  stage: trigger downstream pipelines
  script:
    - bash ./ci-common/downstream-repos.sh
  only:
    refs:
      - /^master$/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml

